package org.bitbucket.strangerintheq.guiceconfig.test;

import java.util.Properties;

import org.junit.Assert;
import org.junit.Test;

import org.bitbucket.strangerintheq.guiceconfig.KeyGenerator;
import org.bitbucket.strangerintheq.guiceconfig.example.TestConfigModule;
import org.bitbucket.strangerintheq.guiceconfig.example.TestIntegerClass;
import org.bitbucket.strangerintheq.guiceconfig.example.TestProperty;
import org.bitbucket.strangerintheq.guiceconfig.example.TestStringClass;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class BindingTest {

    @Test
    public void testIntegerBinding() throws Exception {
        int testValue = 42;
        TestIntegerClass testClass = createTestBindingClassInstance( String.valueOf( testValue ), TestIntegerClass.class );
        Assert.assertTrue( testValue == testClass.getInjectedValue() );
    }
    
    @Test
    public void testStringBinding() throws Exception {
        String testValue = "testValue";
        TestStringClass testClass = createTestBindingClassInstance(  testValue, TestStringClass.class );
        Assert.assertEquals( testValue, testClass.getInjectedValue() );
    }

    private <T> T createTestBindingClassInstance(  String testValue, Class<T> clazz ) {
        Properties props = new Properties();
        String key = KeyGenerator.createKey( TestProperty.TEST_KEY );
        props.put( key, testValue );
        Injector injector = Guice.createInjector( new TestConfigModule( props, TestProperty.values() )  );
        return injector.getInstance( clazz );
    }

}
