package org.bitbucket.strangerintheq.guiceconfig.test;

import org.bitbucket.strangerintheq.guiceconfig.KeyGenerator;
import org.junit.Assert;
import org.junit.Test;

import org.bitbucket.strangerintheq.guiceconfig.example.TestProperty;

public class KeyGeneratorTest {
    
    @Test( expected = NullPointerException.class )
    public void testNullArg() throws Exception {
        KeyGenerator.createKey(null);
    }
    
    @Test
    public void testGenerator() throws Exception {
        String key = KeyGenerator.createKey( TestProperty.TEST_KEY );
        Assert.assertEquals( key, "test.key" );
    }
}
