package org.bitbucket.strangerintheq.guiceconfig.test;

import org.bitbucket.strangerintheq.guiceconfig.BindingAnnotationBase;
import org.junit.Assert;
import org.junit.Test;

import org.bitbucket.strangerintheq.guiceconfig.example.TestConfig;
import org.bitbucket.strangerintheq.guiceconfig.example.TestProperty;

public class AnnotationBaseTest {
    
    private final Class<TestConfig> annotationType = TestConfig.class;
    
    private final TestProperty key = TestProperty.TEST_KEY;

    @Test
    public void testValue() throws Exception {
        Assert.assertEquals( key, create().value() );
    }
    
    @Test
    public void testGetAnnotationType() throws Exception {
        Assert.assertEquals( annotationType, create().annotationType() );
    }
    
    @Test
    public void testHashCodeOfAnnotationLoadedByJVM() throws Exception {
        Assert.assertEquals( "implementation on BindingAnnotationBase.hashCode must match with java.lang.annotation.Annotation#hashCode() implementation", 
                create().hashCode(), 127 * "value".hashCode() ^ key.hashCode() );
    }
    
    @Test
    public void testHashCodeReflexion() throws Exception {
        Assert.assertEquals( create().hashCode(), create().hashCode() );
    }
    
    @Test
    public void testEqualsWithSameTypeObject() throws Exception {
        Assert.assertEquals( create(), create() );
    }
    
    @Test
    public void testEqualsWithOtherTypeObject() throws Exception {
        Assert.assertNotEquals( create(), "" );
    }
    
    private BindingAnnotationBase<TestProperty> create() {
        return new BindingAnnotationBase<TestProperty>( key, annotationType ) {};
    }
}
