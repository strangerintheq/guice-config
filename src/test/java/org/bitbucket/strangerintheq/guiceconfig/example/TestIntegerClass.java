package org.bitbucket.strangerintheq.guiceconfig.example;

import com.google.inject.Inject;

public class TestIntegerClass {

    @Inject
    @TestConfig( TestProperty.TEST_KEY )
    private Integer injectedValue;
    
    public Integer getInjectedValue() {
        return injectedValue;
    } 

}
