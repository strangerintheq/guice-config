package org.bitbucket.strangerintheq.guiceconfig.example;

import com.google.inject.Inject;

public class TestStringClass {

    @Inject
    @TestConfig( TestProperty.TEST_KEY )
    private String injectedValue;
    
    public String getInjectedValue() {
        return injectedValue;
    } 

}
