package org.bitbucket.strangerintheq.guiceconfig.example;

import java.lang.annotation.Annotation;
import java.util.Properties;

import org.bitbucket.strangerintheq.guiceconfig.GuiceConfigModule;
import org.bitbucket.strangerintheq.guiceconfig.BindingAnnotationBase;

public class TestConfigModule extends GuiceConfigModule<TestProperty> {

    public TestConfigModule( Properties props, TestProperty[] keys ) {
        super( props, keys );
    }

    @Override
    protected Annotation createBindingAnnotation( TestProperty prop ) {
        return new BindingAnnotation( prop );
    }

    private static class BindingAnnotation extends BindingAnnotationBase<TestProperty> implements TestConfig {
        public BindingAnnotation( TestProperty bindingKey ) {
            super( bindingKey, TestConfig.class );
        }
    }
}
