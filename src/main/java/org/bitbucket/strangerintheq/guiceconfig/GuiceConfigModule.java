package org.bitbucket.strangerintheq.guiceconfig;

import java.lang.annotation.Annotation;
import java.util.Properties;

import com.google.inject.AbstractModule;
import com.google.inject.Key;

/**
 * usage:
 * <pre>
 * public class TestStringClass {
 * 
 *     &#064Inject
 *     &#064TestConfig( TestProperty.TEST_KEY )
 *     private String injectedValue;
 *     
 *     public String getInjectedValue() {
 *         return injectedValue;
 *     } 
 * }
 * </pre>
 * 
 * config module example:
 * <pre>
 * public class TestConfigModule extends GuiceConfigModule<TestProperty> {
 *
 *     public TestConfigModule( Properties props, TestProperty[] keys ) {
 *         super( props, keys );
 *     }
 * 
 *     &#064Override
 *     protected Annotation createBindingAnnotation( TestProperty prop ) {
 *         return new BindingAnnotation( prop );
 *     }
 *
 *     private static class BindingAnnotation extends BindingAnnotationBase<TestProperty> implements TestConfig {
 *     
 *         public BindingAnnotation( TestProperty bindingKey ) {
 *             super( bindingKey, TestConfig.class );
 *         }
 *     }
 * }
 * </pre>
 * 
 * binding annotation example: 
 * <pre>
 * &#064com.google.inject.BindingAnnotation
 * &#064java.lang.annotation.Retention( RetentionPolicy.RUNTIME )
 * &#064java.lang.annotation.Target( { ElementType.FIELD, ElementType.PARAMETER } )
 * public &#064interface TestConfig {
 *     TestProperty value();
 * }
 * </pre>
 * 
 * enum example:
 * <pre>
 * public enum TestProperty {
 *     TEST_KEY
 * }
 * </pre>
 * 
 * 
 * @author Balashev Konstantin
 */
public abstract class GuiceConfigModule<T extends Enum<?>> extends AbstractModule {

    /** Properties */
    private final Properties props;
    
    /** Keys */
    private final T[] keys;

    /**
     * Base class for guice configuration module
     * @param props properties with keys and values
     * @param keys enumerations with keys representation
     */
    public GuiceConfigModule( Properties props, T[] keys ) {
        this.props = props;
        this.keys = keys;
    }

    /**
     * @see com.google.inject.AbstractModule#configure()
     */
    @Override
    protected void configure() {
        for ( T prop :  keys ) {
            String key = KeyGenerator.createKey( prop );
            Object value = props.getProperty( key );
            if ( null == value ) {
                addError( "property '" + key + "' was not defined'" );
            } else {
                bind( prop, value.toString() );
            }
        }
    }

    /**
     * bind parameter
     * @param prop key
     * @param value value
     */
    protected void bind( T prop, String value ) {
        Annotation bindingAnnotation = createBindingAnnotation( prop );
        bindString( value, bindingAnnotation );
        bindInteger( value, bindingAnnotation );
    }
    
    /**
     * Must create BindingAnnotationBase subtype that implement our binding annotation intercafe
     * @see com.google.inject.BindingAnnotation
     * @param prop enum property for binding annotation
     * @return binding annotation
     */
    protected abstract Annotation createBindingAnnotation( T prop );

    /**
     * Bind String.class with specified binding annotation to string instance
     * @param value string instance
     * @param bindingAnnotation binding annotation instance
     */
    protected void bindString( String value, Annotation bindingAnnotation ) {
        Key<String> key = Key.get( String.class , bindingAnnotation );
        bind( key ).toInstance( value );
    }
    
    /**
     * Bind Integer.class with specified binding annotation to string instance
     * @param value integer as instance
     * @param bindingAnnotation binding annotation instance
     */
    protected void bindInteger( String value, Annotation bindingAnnotation) {
        try {
            Integer intValue = Integer.valueOf( value );
            Key<Integer> key = Key.get( Integer.class , bindingAnnotation );
            bind( key ).toInstance( intValue );
        } catch ( NumberFormatException e ) {
            // no logging needed
        }
    }
}
