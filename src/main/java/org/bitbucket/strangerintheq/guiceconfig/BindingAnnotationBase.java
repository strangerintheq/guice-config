package org.bitbucket.strangerintheq.guiceconfig;

import java.lang.annotation.Annotation;

/**
 * Base class for binding annotation<br>
 * usage:
 * <pre>
 * private static class BindingAnnotation extends BindingAnnotationBase<TestProperty> implements TestConfig {
 *     public BindingAnnotation( TestProperty bindingKey ) {
 *             super( bindingKey, TestConfig.class );
 *     }
 * }
 * </pre>
 *
 * binding annotation example: 
 * <pre>
 * &#064com.google.inject.BindingAnnotation
 * &#064java.lang.annotation.Retention( RetentionPolicy.RUNTIME )
 * &#064java.lang.annotation.Target( { ElementType.FIELD, ElementType.PARAMETER } )
 * public &#064interface TestConfig {
 *     TestProperty value();
 * }
 * </pre>
 * 
 * enum example:
 * <pre>
 * public enum TestProperty {
 *     TEST_KEY
 * }
 * </pre>
 * @author Balashev Konstantin
 */
public abstract class BindingAnnotationBase<T extends Enum<?>> {
    
    /** binding key */
    private final T bindingKey;
    
    /** annotation type */
    private final Class<? extends Annotation> annotationType;

    public BindingAnnotationBase( T bindingKey, Class<? extends Annotation> annotationType ) {
        this.bindingKey = bindingKey;
        this.annotationType = annotationType;
    }
    
    /**
     * @return annotation value
     */
    public final T value() {
        return bindingKey;
    }
    
    /**
     * @see java.lang.annotation.Annotation#annotationType()
     */
    public final Class<? extends Annotation> annotationType() {
        return annotationType;
    }

    /**
     * @see java.lang.annotation.Annotation#hashCode()
     */
    @Override
    public final int hashCode() {
        return 127 * "value".hashCode() ^ value().hashCode();
    }

    /**
     * @see Object#equals(Object)
     */
    @Override
    public final boolean equals( Object o ) {
        if ( o instanceof BindingAnnotationBase ) {
            BindingAnnotationBase other = (BindingAnnotationBase) o;
            return value().equals( other.value() );
        }
        return false;
    }
    
}
