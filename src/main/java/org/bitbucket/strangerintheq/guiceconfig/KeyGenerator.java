package org.bitbucket.strangerintheq.guiceconfig;

/**
 * Generates keys from enum config binding
 * @author Balashev Konstantin
 */
public class KeyGenerator {

    /**
     * Create a key
     * @param enumeration binding enum
     * @return config key for enumeration name
     */
    public static String createKey( Enum<?> enumeration ) {
        return enumeration.name()
                          .replaceAll( "_", "." )
                          .toLowerCase();
    }
    
}
